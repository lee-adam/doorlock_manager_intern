import React from 'react';
import { Route, useHistory, BrowserRouter } from 'react-router-dom';
import Signup from './components/Signup';

const App = () => {
  let history = useHistory();
  return (
    <BrowserRouter>
      <Route path="/" exact history={history} component={Signup} />
    </BrowserRouter>
  );
};

export default App;
