import React, { useState } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faLock, faEnvelope } from '@fortawesome/free-solid-svg-icons';
import { ArrowRight } from 'react-feather';
import { useCookie } from './hooks';
import {
  Container,
  Title,
  InputContainer,
  Input,
  LoginBtn,
  BtnContainer,
} from './styles/Signup.styles';

const Signup = () => {
  const [apiKeyCookie, setApiKeyCookie] = useCookie('api_key', '');
  const [emailText, setEmailText] = useState('');
  return (
    <Container>
      <Title>Skywatch Welcome Page</Title>
      <InputContainer>
        <FontAwesomeIcon icon={faEnvelope} color={'#fff'} />
        <Input
          placeholder={'Email'}
          value={emailText}
          onChange={e => {
            setEmailText(e.target.value);
          }}
        />
      </InputContainer>
      <BtnContainer>
        <LoginBtn onClick={() => {}}>
          <ArrowRight color={'#036084'} size={30} />
        </LoginBtn>
      </BtnContainer>
    </Container>
  );
};

export default Signup;
