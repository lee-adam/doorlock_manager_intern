import styled from '@emotion/styled';
import { css } from '@emotion/core';

export const Container = styled.div`
  width: 100%;
  height: 100vh;
  flex-direction: column;
  background-color: #2f93cf;
  display: flex;
  align-items: center;
  justify-content: center;
`;

export const Title = styled.div`
  position: absolute;
  top: 30px;
  font-family: Noto Sans TC;
  font-style: normal;
  font-weight: blod;
  font-size: 40px;
  line-height: 50px;
  letter-spacing: 0.18px;
  color: #ffffff;
`;

export const InputContainer = styled.div`
  display: flex;
  flex-direction: row;
  width: 310px;
  border-width: 1px;
  border-bottom-style: solid;
  border-color: #fff;
  svg {
    margin-top: 5px;
  }
`;

export const Input = styled.input`
  margin-left: 12px;
  width: 265px;
  background-color: #2f93cf;
  font-family: Noto Sans TC;
  font-size: 16px;
  line-height: 24px;
  color: #fff;
  outline: none;
  border-width: 0px;

  ::placeholder {
    color: #fff;
    opacity: 0.5;
  }
`;

export const BtnContainer = styled.div`
  display: flex;
  flex-direction: row;
  width: 265px;
  justify-content: flex-end;
  align-items: center;
`;

export const LoginBtn = styled.button`
  height: 48px;
  width: 48px;
  border-radius: 50px;
  border-width: 0px;
  outline: none;
  float: right;
  display: flex;
  justify-content: center;
  cursor: pointer;
  background-color: #ffcf77;
  margin-top: 11px;

  transform: translate3d(0, 0, 0);
  :after {
    content: '';
    display: block;
    position: absolute;
    width: 100%;
    height: 100%;
    top: 0;
    left: 0;
    background: radial-gradient(circle, #fff 10%, transparent 10.01%);
    background-position: 50%;
    transform: scale(7.5, 7.5);
    opacity: 0;
    transition: transform 0.3s, opacity 1s;
  }
  :active:after {
    transform: scale(0, 0);
    opacity: 0.3;
    transition: 0s;
  }
`;
